// // A new UUID must be generated for the first run and re-used for your Job DSL, the plugin updates jobs based on ID
// UUID uuid = UUID.fromString("5f8becfc-e881-4ca7-9e5e-06de8acfa15c") // generate one @ https://www.uuidgenerator.net

// multibranchPipelineJob("${Name}") {
//     displayName "${Name}"
//     description "Builds ${Name}"
//     configure {
//         it / sources / 'data' / 'jenkins.branch.BranchSource' << {
//             source(class: 'jenkins.plugins.git.GitSCMSource') {
//                 id(uuid)
//                 remote("<Git repo url>")
//                 credentialsId("<Credentials ID for Git repo>")
//                 includes('*')
//                 excludes('')
//                 ignoreOnPushNotifications('false')
//                 traits {
//                     'jenkins.plugins.git.traits.BranchDiscoveryTrait'()
//                 }
//             }
//         }
//         // customise the branch project factory
//         it / factory(class: "org.jenkinsci.plugins.workflow.multibranch.WorkflowBranchProjectFactory") << {
//             owner(class:"org.jenkinsci.plugins.workflow.multibranch.WorkflowMultiBranchProject", reference: "../..")
//             scriptPath("${jenkinsfile}")
//         }
//     }
// }
///////////////////////

// multibranchPipelineJob("generated/"+name.toString()) {
//     branchSources {
//     //     branchSource {
//     //       source{
//     //         git {
//     //           id("5f8becfc-e881-4ca7-9e5e-06de8acfa15c") // IMPORTANT: use a constant and unique identifier
//     //           remote('https://gitlab.com/marti700/testpipelines.git')
//     //           credentialsId('public-gitlab1')
//     //           traits {
//     //                   gitBranchDiscovery()
//     //               }
//     //         }

//     //       }

//     //     }
//     // }
//         git {
//             id("5f8becfc-e881-4ca7-9e5e-06de8acfa15c") // IMPORTANT: use a constant and unique identifier
//             remote('https://gitlab.com/marti700/testpipelines.git')
//             credentialsId('public-gitlab1')
//             includes('*')
//             excludes('')
//                     // withTraits(BranchDiscoveryTrait)
//                     withTraits(jenkins.plugins.git.traits.BranchDiscoveryTrait.class)
//         }
//     }

//         // By default, Jenkins will trigger builds as it detects changes on the source repository. We want
//         // to avoid that since we will trigger child pipelines on our own only when relevant.
//         // buildStrategies {
//         //     skipInitialBuildOnFirstBranchIndexing()
//         // }
//         // strategy {
//         //     defaultBranchPropertyStrategy {
//         //       props {
//         //         noTriggerBranchProperty()
//         //       }
//         //     }
//         // }
//     triggers {
//       gitlab{
//         // sourceBranchRegex("*")
//         triggerOnlyIfNewCommitsPushed(true)
//       }
//     }
//     factory {
//             workflowBranchProjectFactory {
//               scriptPath(jenkinsfile.toString())
//         }
//     }
//     orphanedItemStrategy {
//         discardOldItems {
//             numToKeep(20)
//         }
//     }
// }

UUID uuid = UUID.fromString("5f8becfc-e881-4ca7-9e5e-06de8acfa15c") // generate one @ https://www.uuidgenerator.net

multibranchPipelineJob("generated/${name}") {
    displayName "${name}"
    description "Builds ${name}"
    configure {
        it / sources / 'data' / 'jenkins.branch.BranchSource' << {
            source(class: 'jenkins.plugins.git.GitSCMSource') {
                id(uuid)
                remote("https://gitlab.com/marti700/testpipelines.git")
                credentialsId("public-gitlab1")
                includes('*')
                excludes('')
                ignoreOnPushNotifications('false')
                traits {
                    'jenkins.plugins.git.traits.BranchDiscoveryTrait'()
                }
            }
        }
        // customise the branch project factory
        it / factory(class: "org.jenkinsci.plugins.workflow.multibranch.WorkflowBranchProjectFactory") << {
            owner(class:"org.jenkinsci.plugins.workflow.multibranch.WorkflowMultiBranchProject", reference: "../..")
            scriptPath("${jenkinsfile}")
        }
    }
}